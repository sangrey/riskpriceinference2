#!/usr/bin/env python
# coding: utf-8

import numpy as np
import pandas as pd
import volpriceinference as vl
import matplotlib as mpl
import seaborn as sns
import logging
import sys
from scipy import special, stats
from multiprocessing import get_context

mpl.style.use('seaborn-talk')
np.set_printoptions(precision=3)

logging.captureWarnings(True)
root_logger = logging.getLogger()
root_logger.setLevel(logging.INFO)
info_handler = logging.FileHandler('../logging.tmp.txt')
info_handler.addFilter(lambda record: record.levelno <= logging.WARNING)
error_handler = logging.StreamHandler(sys.stderr)
error_handler.addFilter(lambda record: record.levelno > logging.WARNING)
root_logger.addHandler(info_handler)
root_logger.addHandler(error_handler)

bounds = {'zeta1': {'max': 0, 'min':-40, 'dim':50}, 'phi': {'max': -.2, 'min':-.8, 'dim':10}, 
          'zeta2': {'max':4, 'min': 0, 'dim':25}}
innov_dim = 2000

vl.__version__
# ## Load the data

data = pd.read_hdf('../data/spy_daily_estimates_heavy.hdf')

## Plot the data 
data_for_plot = data.copy()
data_for_plot.vol *= 252**2
data_for_plot.rtn *= 252
data_for_plot.mu *= 252**2

fig1, ax1 = mpl.pyplot.subplots()
data_for_plot['rtn'].plot(ax=ax1, color='blue', alpha=.5, label="$r_{t+1}$")
data_for_plot['vol'].apply(np.sqrt).plot(ax=ax1, label='$RV$', color='red')
data_for_plot['mu'].apply(np.sqrt).plot(ax=ax1, label='$\mu$', color='purple')
ax1.legend()
fig1.savefig('../doc/figures/time_series.tmp.png', pad_inches=0, bbox_inches='tight')

# Plot the distributions.
joint_grid = sns.JointGrid(x='mu', y='rtn', data=data_for_plot) #, xlim=[-.1, 3], ylim=[-2, 2])
joint_grid.plot_joint(sns.regplot, ci=None, color='purple', scatter_kws={"alpha":.5})
sns.kdeplot(data['mu'], ax=joint_grid.ax_marg_x, legend=None, shade=True, color='red')
sns.kdeplot(data['rtn'], ax=joint_grid.ax_marg_y, legend=None, shade=True, color='blue', vertical=True)
joint_grid.set_axis_labels("$\mu$")
joint_grid.ax_marg_x.set_xticks(list(joint_grid.ax_marg_x.get_xticks())[::2])

joint_grid.ax_marg_x.set_xticklabels(["{:,.0f}".format(val) for val in joint_grid.ax_marg_x.get_xticks()])
joint_grid.savefig('../doc/figures/joint_dist.tmp.png', pad_inches=0, bbox_inches='tight')

joint_grid = sns.JointGrid(x='vol', y='rtn', data=data_for_plot) #, xlim=[-.1, 3], ylim=[-2, 2])
joint_grid.plot_joint(sns.regplot, ci=None, color='purple', scatter_kws={"alpha":.5})
sns.kdeplot(data['vol'], ax=joint_grid.ax_marg_x, legend=None, shade=True, color='red')
sns.kdeplot(data['rtn'], ax=joint_grid.ax_marg_y, legend=None, shade=True, color='blue', vertical=True)
joint_grid.set_axis_labels("$\sigma^2_{t+1}$","$r_{t+1}$")
joint_grid.ax_marg_x.set_xticks(list(joint_grid.ax_marg_x.get_xticks())[::2])

joint_grid.ax_marg_x.set_xticklabels(["{:,.0f}".format(val) for val in joint_grid.ax_marg_x.get_xticks()])
joint_grid.savefig('../doc/figures/joint_dist.tmp.png', pad_inches=0, bbox_inches='tight')

print(data_for_plot.describe())

print(stats.kurtosis(data_for_plot, fisher=False))

print(stats.skew(data_for_plot))

print(data_for_plot.corr())

# ## We now estimate the volatility paramters.

omega, omega_cov = vl.estimate_params_strong_id(data[['rtn', 'mu']].rename(columns={'mu':'vol'}), bounds=bounds)

print(omega)

estimates, covariance = vl.estimate_params(data[['rtn', 'mu']].rename(columns={'mu':'vol'}), *vl.compute_vol_gmm(data.vol))

print(estimates)

# ## I compute the point estimates and confidence intervals for the volatility parameters.

print((252 * np.exp(estimates['log_scale']),
252 * np.exp(estimates['log_scale'] - 1.96 * covariance.loc['log_scale', 'log_scale']**.5),
 252 * np.exp(estimates['log_scale'] + 1.96 * covariance.loc['log_scale', 'log_scale']**.5)))

# delta

log_mean = estimates['log_both'] - estimates['log_scale']
log_var = (covariance.loc['log_scale', 'log_scale'] + covariance.loc['log_both', 'log_both'] 
           - 2 * covariance.loc['log_scale', 'log_both'])
print(252 * np.exp(log_mean),
 252 * np.exp(log_mean - 1.96 * log_var**.5),
 252 * np.exp(log_mean + 1.96 * log_var**.5)
)

# ### rho

mean = estimates['logit_rho']
std = covariance.loc['logit_rho', 'logit_rho']**.5
print(special.expit(mean), special.expit(mean - 1.96 * std), special.expit(mean + 1.96*std))

print(covariance)

omega2, omega_cov2 = vl.estimate_params(data)

qlr_stats = vl.compute_qlr_stats(omega=omega2, omega_cov=omega_cov2, bounds=bounds,use_tqdm=True)

qlr_stats.to_json(f'../results/qlr_stats_on_data14_{innov_dim}.json')

qlr_draws = vl.compute_qlr_sim(omega=omega2, omega_cov=omega_cov2, bounds=bounds, use_tqdm=True,
                               alpha=0.05, innov_dim=innov_dim) 

print(qlr_draws.phi.unique())

qlr_draws.to_json(f'../results/qlr_draws_on_data14_{innov_dim}.json')

qlr_draws = pd.read_json(f'../results/qlr_draws_on_data14_{innov_dim}.json')

qlr_stats = pd.read_json(f'../results/qlr_stats_on_data14_{innov_dim}.json')

print(np.sort(qlr_draws.phi.unique()) - np.sort(qlr_stats.phi.unique()))

merged_values = vl.merge_draws_and_sims(qlr_stats=qlr_stats, qlr_draws=qlr_draws)

accepted_vals2 = merged_values.where(lambda x: np.isfinite(x)).dropna().query('qlr_stats < qlr_draws')

print(accepted_vals2)

print(accepted_vals2.dropna().max())

print(accepted_vals2.min())

accepted_vals = merged_values.query('qlr_stats < qlr_draws')

def constraint(scale, psi, zeta, equity_price):
    
    vol_price  = -1 / scale - ((psi * (equity_price-1) + zeta/2 * (equity_price-1)**2))
    
    return  vol_price

print(accepted_vals.min())

print(accepted_vals.max())

print(stats.chi2.ppf(df=4, q=.95))

## Plot the results
var1 = 'zeta1'
var2 = 'zeta2'

fig3, ax3 = mpl.pyplot.subplots()
pi_patch_width = 1.2 * (bounds[var1]['max'] - bounds[var1]['min']) / bounds[var1]['dim']
theta_patch_height = 1.2 * (bounds[var2]['max'] - bounds[var2]['min']) / bounds[var2]['dim']
   
for row in accepted_vals.itertuples(index=False):
    patch = mpl.patches.Rectangle((getattr(row, var1), getattr(row,var2)), width=pi_patch_width, height=theta_patch_height,
                                  fill=True, color='purple')
    ax3.add_patch(patch)
    
max_y = bounds[var2]['max']
min_y = bounds[var2]['min']
max_x = bounds[var1]['max']
min_x = bounds[var1]['min']

ax3.axvline(0,  ymin=0, ymax=1, color='black')
ax3.axhline(0,  xmin=1, xmax=0, color='black')

white_rect1 = mpl.patches.Rectangle((0,-1), 2, max_y + 2, angle=0.0, color='white', fill=True)
white_rect2 = mpl.patches.Rectangle((min_x-1,-1), abs(min_x) + 2, 1, angle=0.0, color='white', fill=True)
ax3.add_patch(white_rect1)
ax3.add_patch(white_rect2)
ax3.set_ylim([1.5 * min_y, 1.5 * max_y])
ax3.set_xlim([1.5 * min_x, 1.5 * max_x])
ax3.set_ylabel(var1)
ax3.set_xlabel(var2)

fig3.savefig(f'doc/figures/qlr_confidence_region_{innov_dim}2.tmp.png', frameon=False, pad_inches=0, 
             bbox_inches='tight')

fig4, ax4 = mpl.pyplot.subplots()
eigvals, eigvectors = np.linalg.eig(omega_cov.loc[['zeta1', 'zeta2'], ['zeta1', 'zeta2']])
root_eigvals = np.sqrt(eigvals)

corr = omega_cov.loc['zeta2', 'zeta1'] / np.sqrt(omega_cov.loc['zeta2', 'zeta2'] * omega_cov.loc['zeta1', 'zeta1'])
standard_patch = mpl.patches.Ellipse(xy=(omega['zeta1'], omega['zeta2']), 
                                     width=1.96 * 2 * root_eigvals[0], 
                                     height=1.96 * 2 * root_eigvals[1], 
                                     angle=np.rad2deg(np.arccos(eigvectors[0,0])), color='purple')
ax4.add_patch(standard_patch)
   
max_y = bounds['zeta2']['max']
min_x = bounds['zeta1']['min']

ax4.axvline(0,  ymin=0, ymax=1, color='black')
ax4.axhline(0,  xmin=1, xmax=0, color='black')

white_rect1 = mpl.patches.Rectangle((0,-1), 2, max_y + 2, angle=0.0, color='white', fill=True)
white_rect2 = mpl.patches.Rectangle((-35,-1), abs(min_x) + 2, 1, angle=0.0, color='white', fill=True)
ax4.add_patch(white_rect1)
ax4.add_patch(white_rect2)
ax4.set_ylim([-.2, 10])
ax4.set_xlim([-35, .2])
ax4.set_ylabel(r'$\kappa$')
ax4.set_xlabel(r'$\pi$')

fig4.savefig(f'doc/figures/standard_confidence_region.tmp.pdf', frameon=False, pad_inches=0, 
             bbox_inches='tight', transparent=True)

