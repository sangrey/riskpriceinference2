"""Ensures that we have the correct implmentation of the model's functions."""
import sympy as sym
from hypothesis import given, settings, strategies as st
import numpy as np
import pytest
import volpriceinference as vl

pytestmark = pytest.mark.filterwarnings("ignore:.*U.*mode is deprecated:DeprecationWarning")

finite_floats = st.floats(min_value=-10, max_value=10)
positive_floats = st.floats(min_value=.1, max_value=10)
negative_floats = st.floats(min_value=-10, max_value=-.1)
phi_floats = st.floats(min_value=-.9, max_value=-.1)
log_floats = st.floats(min_value=-5, max_value=2)


# Define the gradient of the link function with respect to the reduced form paramters.
_link_grad_sym = sym.powsimp(sym.expand(sym.Matrix([
    vl.volprice._link_sym.jacobian([vl.beta, vl.gamma, vl.log_both,
                                    vl.log_scale, vl.logit_rho, vl.psi,
                                    vl.xi])])))

omega_cov = sym.MatrixSymbol('omega_cov', _link_grad_sym.shape[1], _link_grad_sym.shape[1])

@given(finite_floats, log_floats, positive_floats)
@settings(max_examples=80)
def test_A_func(logit_rho, log_scale, x):
    """Ensure the two A_func implementations return the same value."""
    val1 = sym.N(vl.volprice._A_func.replace(vl.logit_rho, logit_rho).replace(
        vl.log_scale, log_scale).replace(vl.volprice._x, x))
    val2 = vl.A_func(logit_rho=logit_rho, log_scale=log_scale, x=x)

    if np.isfinite(val2):
        assert np.isclose(float(np.real(val1)), float(np.real(val2)), equal_nan=True, rtol=1e-3),\
            f"The two implementations return different values: {val1} and {val2}"



@given(phi_floats, negative_floats, positive_floats, finite_floats, finite_floats, log_floats,
       log_floats, finite_floats, finite_floats)
@settings(max_examples=60)
def test_link_total(phi, zeta1, zeta2, beta, gamma, log_both, log_scale, logit_rho, psi):
    """Ensure the two implementations have the same link functions."""

    xi = 1 - phi**2

    args = {vl.volprice.phi: phi, vl.volprice.zeta1: zeta1, vl.volprice.zeta2: zeta2, vl.volprice.beta: beta,
            vl.volprice.gamma: gamma, vl.volprice.log_both: log_both,
            vl.volprice.log_scale: log_scale, vl.volprice.logit_rho: logit_rho, vl.volprice.psi: psi,
            vl.volprice.xi: xi}

    val1 = np.real(np.array(sym.N(vl.volprice._link_sym.xreplace(args))).astype(np.complex))
    val2 = vl.link_total(phi=phi, zeta1=zeta1, zeta2=zeta2, beta=beta, gamma=gamma, logit_rho=logit_rho,
                         log_both=log_both, log_scale=log_scale, psi=psi, xi=xi)

    if np.all(np.isfinite(val2)) and np.all(abs(val2) <= 1e4):
        assert np.allclose(np.ravel(val1), np.ravel(val2), rtol=1e-3, equal_nan=True), \
            f"The two implementations return different values: {val1} and {val2}"


@given(phi_floats, negative_floats, positive_floats, log_floats, log_floats, finite_floats, finite_floats)
@settings(deadline=1000, max_examples=20)
def test_link_gradient(phi, zeta1, zeta2, log_both, log_scale, logit_rho, psi):
    """Ensure the two implementations have the same link function gradient gradientsss."""

    xi = 1 - phi**2

    args = {vl.volprice.phi: phi, vl.volprice.zeta1: zeta1, vl.volprice.zeta2: zeta2, vl.volprice.log_both: log_both,
            vl.volprice.log_scale: log_scale, vl.volprice.logit_rho: logit_rho, vl.volprice.psi: psi,
            vl.volprice.xi: xi}

    val1 = np.real(np.array(sym.N(_link_grad_sym.xreplace(args))).astype(np.complex))
    val2 = vl.link_jacobian(phi=phi, zeta1=zeta1, zeta2=zeta2, logit_rho=logit_rho,
                            log_both=log_both, log_scale=log_scale, psi=psi)

    if np.all(np.isfinite(val2)) and np.all(abs(val2) <= 1e4):
        assert np.allclose(np.ravel(val1), np.ravel(val2), rtol=1e-3, equal_nan=True), \
            f"The two implementations return different values: {val1} and {val2}"
