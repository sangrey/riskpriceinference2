#include <stdexcept>
#include <cmath>
#include <cassert>
#include <random>
#include <algorithm>
#include <string>
#include <stdexcept>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h> 
#include "pybind11/operators.h"
#include <pybind11/iostream.h>
#include <arma_wrapper.h>

namespace py = pybind11;
using namespace pybind11::literals;
using namespace std::string_literals;
using stream_redirect = py::call_guard<py::scoped_ostream_redirect>;                                               
using aw::dmat;
using aw::dvec;

double logistic(double x) {

    return 1.0 / (1 + std::exp(-x));
}

double logit(double x) {

    return std::log(x)  - std::log1p(-x); 
}


/*
 * This function solves takes the lower cholesky factor of a matrix (A) and solves the system A \ B. 
 * It is inspired by to the cho_solve function from scipy.
 */
inline
dmat cho_solve(const dmat &lroot, const dmat &mat2) {
    return arma::solve(arma::trimatu(lroot.t()), arma::solve(arma::trimatl(lroot), mat2));
}


/* 
 * Initializes a random generator in a thread_save way to be used globally througout the entire library. 
 */
inline
std::mt19937_64 initialize_mt_generator() { 

    constexpr size_t state_size = std::mt19937_64::state_size * std::mt19937_64::word_size / 32;
    std::vector<uint32_t> random_data(state_size);
    std::random_device source{}; 
    std::generate(random_data.begin(), random_data.end(), std::ref(source));
    std::seed_seq seeds(random_data.begin(), random_data.end());
    std::mt19937_64 engine(seeds);
    
    return engine; 

}

std::vector<double> simulate_autoregressive_gamma(double delta, double rho, double scale, size_t time_dim, double
        initial_point) {
    
    assert (time_dim > 1);

    std::vector<double> draws;
    draws.reserve(time_dim+1);

    /* We start at the initial point. */
    draws.push_back(initial_point);
    auto generator = initialize_mt_generator();

    for(size_t idx=0; idx<time_dim; ++idx) {
        
        /* The conditional distribution of an ARG(1) process is non-centered Gamma, which has a representation as
         * a Poisson mixture of Gamma */
          std::poisson_distribution<int> poi_dist(rho * draws.back() / scale);
          int latent_var = poi_dist(generator);
          std::gamma_distribution<double> gamma_dist(delta + latent_var, scale);
        /* I force the draws to be bounded away from zero. Otherwise we can get stuck there.*/
          draws.push_back(std::max(gamma_dist(generator), 1e-5));
    }

    std::vector<double> return_draws(draws.begin() + 1, draws.end());

    return return_draws;
}

std::vector<double> threadsafe_gaussian_rvs(size_t time_dim) {

    std::normal_distribution<double> dist(0,1);
    auto generator = initialize_mt_generator();

    std::vector<double> return_draws(time_dim);

    for (auto& x : return_draws) {
        x = dist(generator);
    }
    
    return return_draws;
}

double A_func(double x, double logit_rho, double log_scale) {

    double scale =  std::exp(log_scale);
    double rho = logistic(logit_rho); 

    double val = rho * x / (1 + scale * x);

    return val;

}

double A_diff1(double x, double logit_rho, double log_scale) {
    /* Compute the derivatiive of the A function with respect to x. */
    
    double scale =  std::exp(log_scale);
    double rho = logistic(logit_rho); 
    double numerator = rho;
    double denominator = std::pow(1 + scale * x, 2.0);

    return numerator / denominator;

}

double A_diff2(double x, double logit_rho, double log_scale) {
    /* Compute the derivatiive of the A function with respecto to logit_rho. */
    
    double scale =  std::exp(log_scale);

    double const_ratio = x / (1 + scale * x); 
    /* The derivative of the logistic function satisfies f'(x) = f(x) ( 1 - f(x))  */
    double logistic_prime = logistic(logit_rho) * (1 - logistic(logit_rho));

    return const_ratio * logistic_prime;

}

double A_diff3(double x, double logit_rho, double log_scale) {
    /* Compute the derivatiive of the A function with respecto to log_scale. */
    
    double rho = logistic(logit_rho);

    double numerator =  rho * x * x * std::exp(log_scale);
    double denominator_in =  1 + std::exp(log_scale) * x;

    return -1.0 * numerator / std::pow(denominator_in, 2.0);

}


double B_func(double x, double log_both, double log_scale) { 
    /* Compute the B_func in the accompanying paper. */

    double delta = std::exp(log_both - log_scale);
    double scale =  std::exp(log_scale);

    return delta * std::log1p(scale * x);
}

double B_diff1(double x, double log_both, double log_scale) {
    /* Compute the derivative of the B_func with respect to the first argument. */

    double delta = std::exp(log_both - log_scale);
    double scale =  std::exp(log_scale);
   
    /* The first term is just std::exp(log_both) */
    return ((delta * scale) / (1 + scale * x));

}

double B_diff2(double x, double log_both, double log_scale) { 
    /* Compute the derivative of B w.r.t. log_both */

    return B_func(x, log_both, log_scale);
}

double B_diff3(double x, double log_both, double log_scale) {
    /* Compute the derivative of B w.r.t. log_scale */

    // The std::exp(log_both) term arises because the exp(log_scale) terms cancel. 
    double part1 = x / (1 + std::exp(log_scale) * x) * std::exp(log_both);

    double part2 = -1.0 * B_func(x, log_both, log_scale);

    return part1 + part2;
}

double alpha_func(double x, double phi, double psi) {

    return psi * x - ((1 - phi * phi) / 2.0) * x * x;

}

double alpha_diff1(double x, double phi, double psi) {

    return  psi - ( 1- phi * phi) * x;

}

double alpha_diff2(double x, double phi)  {

    return phi * x * x; 

}


double alpha_func_diff3(double x) {

    return x;

}


double beta_curvature(double phi, double logit_rho, double log_scale, double psi)  {

// double rho = logistic(logit_rho); 
// double xi = 1 - phi * phi;

 //   (1 - xi) rho - 2 * psi**2 * rho *  scal 
 //   = (1 - (1 - phi**2)) * rho - 2 * psi**2 * rho * scale
 //   = rho * ( phi**2 - 2 * psi**2 * scale) 
 //   

 //   1 - (1 - phi**2)  * rho + psi**2 (- 2 rho c)
 //   = 1 - rho + rho ** phi**2 - 2 *  psi**2 ** rho * c
 //   = 1 + rho * (-1 + phi**2 - 2 * psi**2 * c)
 //   = 1 + logistic(logit_rho) * ( phi**2 - 1 - 2 * psi**2 * exp(log_scale))

    double curvature = -1 * (1 + logistic(logit_rho) * ( std::pow(phi, 2) - 1 - 2 * std::exp(log_scale) * std::pow(psi, 2)));

// double curvature = (logistic(logit_rho) * (phi * phi + 2 * std::exp(log_scale) * psi * psi));

    return curvature;
}


double beta_curvature_diff2(double phi, double logit_rho, double log_scale, double psi) {
  /* 
   * The derivative of beta_curvature with respect to logit_rho
   * Note the derivitive of the logistic function satsifies f'(x) = f(x) (1 - f(x)).
  */ 

    double scale = (std::pow(phi, 2) - 1 - 2 * std::exp(log_scale) * std::pow(psi, 2));
    double rho = logistic(logit_rho); 

    return -1 * rho * (1 - rho) * scale; 

} 


double beta_curvature_diff3(double logit_rho, double log_scale, double psi)  {
    /* 
     * The derivative of beta_curvature with respect to the log_scale.
     * Note the derivitive of the logistic function satsifies f'(x) = f(x) (1 - f(x)).
     */

    double val1 = -1 * (logistic(logit_rho) * (-2 * std::exp(log_scale) * std::pow(psi, 2)));

    return val1;

} 


double beta_curvature_diff4(double logit_rho, double log_scale, double psi)  {
    /* 
     * The derivative of beta_curvature with respect to psi.
     */

    double val1 =  logistic(logit_rho) * (4 * std::exp(log_scale) * psi);

    return val1;

} 


double compute_beta(double zeta1, double phi, double zeta2, double logit_rho,
        double log_scale, double psi) {

    double curvature = beta_curvature(phi, logit_rho, log_scale, psi); 

    double val1 = A_func(zeta1 + alpha_func(zeta2 - 1, phi, psi), logit_rho, log_scale);
    double val2 = A_func(zeta1 + alpha_func(zeta2, phi, psi), logit_rho, log_scale);

    double beta_omega = (val1 - val2); 

    return beta_omega - (zeta2 - .5)  * curvature;

}


std::tuple<double, double, double> beta_gradient(double zeta1, double phi,
        double zeta2, double logit_rho, double log_scale, double psi) {

    double d_logit_rho = A_diff2(zeta1 + alpha_func(zeta2-1, phi, psi), logit_rho, log_scale) - 
                         A_diff2(zeta1 + alpha_func(zeta2, phi, psi), logit_rho, log_scale) 
                         - (zeta2 - .5) * beta_curvature_diff2(phi, logit_rho, log_scale, psi); 

    double d_log_scale = A_diff3(zeta1 + alpha_func(zeta2-1, phi, psi), logit_rho, log_scale) - 
                         A_diff3(zeta1 + alpha_func(zeta2, phi, psi), logit_rho, log_scale) 
                         - (zeta2 - .5) * beta_curvature_diff3(logit_rho, log_scale, psi); 

    double term1 = A_diff1(zeta1 + alpha_func(zeta2-1, phi, psi), logit_rho,
            log_scale) * alpha_func_diff3(zeta2-1);   
    double term2 = A_diff1(zeta1 + alpha_func(zeta2, phi, psi), logit_rho,
            log_scale) * alpha_func_diff3(zeta2);

    double term3 = (zeta2 - .5) * beta_curvature_diff4(logit_rho, log_scale, psi); 
    double d_psi =  term1 - term2 - term3;

    return std::make_tuple(d_logit_rho, d_log_scale, d_psi);
}


double gamma_curvature(double phi, double log_both, double log_scale, double psi) {

    // double xi = 1 - phi * phi;
    // double curvature = -1.0 * (1 - xi) * std::exp(log_both) + std::exp(log_scale + log_both) * std::pow(psi,2);

    double curvature = (1 - std::pow(phi, 2)) * std::exp(log_both) + std::exp(log_scale + log_both) * std::pow(psi, 2);


    return curvature;
}

double compute_gamma(double zeta1, double phi, double zeta2, double log_both, double log_scale, double psi) {

    double val1 = B_func(zeta1 + alpha_func(zeta2 - 1, phi, psi), log_both, log_scale);
    double val2 = B_func(zeta1 + alpha_func(zeta2, phi, psi), log_both, log_scale);

    double gamma_omega =  (val1 - val2);

    double curvature = gamma_curvature(phi, log_both, log_scale, psi); 
    return gamma_omega - (zeta2 - .5) * curvature;
}

double gamma_curvature_diff2(double phi, double log_both, double log_scale, double psi) {
    /* 
     * This is the derivative of gamma_curvature with respect to log_both.
     * It equal gamma_curvature because the derivative of an exponential equals itself and you 
     * can factor out std::exp(log_scale) and then recombine.
     */

    return gamma_curvature(phi, log_both, log_scale, psi);
}

double gamma_curvature_diff3(double log_both, double log_scale, double psi) {
    /* This is the derivative of gamma curvature with respect to log_scale */

    return std::exp(log_scale + log_both) * std::pow(psi,2);
}


double gamma_curvature_diff4(double log_both, double log_scale, double psi) {
    /* This is the derivative of gamma curvature with respect to psi */

        return std::exp(log_scale + log_both) * 2.0 * psi; 
}

std::tuple<double, double, double> gamma_gradient(double zeta1, double phi,
        double zeta2, double log_both, double log_scale, double psi) {
    

    double d_log_both = B_diff2(zeta1 + alpha_func(zeta2-1, phi, psi), log_both, log_scale) - 
                        B_diff2(zeta1 + alpha_func(zeta2, phi, psi), log_both, log_scale) 
                        - (zeta2 - .5) * gamma_curvature_diff2(phi, log_both, log_scale, psi); 

    double d_log_scale = B_diff3(zeta1 + alpha_func(zeta2-1, phi, psi), log_both, log_scale) - 
                         B_diff3(zeta1 + alpha_func(zeta2, phi, psi), log_both, log_scale)  
                        - (zeta2 - .5) * gamma_curvature_diff3(log_both, log_scale, psi); 

    double term1 = B_diff1(zeta1 + alpha_func(zeta2-1, phi, psi), log_both,
            log_scale) * alpha_func_diff3(zeta2-1); 

    double term2 = B_diff1(zeta1 + alpha_func(zeta2, phi, psi), log_both,
            log_scale) * alpha_func_diff3(zeta2);  
    double d_psi = term1 - term2 - (zeta2 - .5) *
        gamma_curvature_diff4(log_both, log_scale, psi); 

    return std::make_tuple(d_log_both, d_log_scale, d_psi);

}

double compute_psi(double phi, double zeta2, double log_scale, double logit_rho) { 

    double rho = logistic(logit_rho); 
    double ratio1 = std::exp(-.5 * log_scale) * std::pow(1 + rho, -.5);
    double val = ratio1 * phi + ( 1.0 - std::pow(phi,2)) * (zeta2 - .5);

    return val;
}

std::tuple<double, double> psi_gradient(double phi, double log_scale, double logit_rho) {

    double rho = logistic(logit_rho); 
    double d_log_scale = -.5 * std::exp(-.5 * log_scale) * std::pow(1 + rho, -.5) * phi;
    double d_logit_rho = -.5 * std::exp(-.5 * log_scale) * std::pow(1 + rho,
            -1.5) * phi * rho * (1 - rho);

    return std::make_tuple(d_log_scale, d_logit_rho);

}

dvec link_total(double phi, double zeta1, double zeta2, double beta, double
        gamma, double log_both, double log_scale, double logit_rho, double psi,
        double xi) {

    double beta_difference = beta - compute_beta(zeta1, phi, zeta2, logit_rho,
            log_scale, psi); 

    double gamma_difference = gamma - compute_gamma(zeta1, phi, zeta2,
            log_both, log_scale, psi); 

    double psi_difference = psi - compute_psi(phi, zeta2, log_scale, logit_rho);
    double zeta_difference = 1 - (xi + phi * phi);

    dvec returnvec{beta_difference, gamma_difference, psi_difference, zeta_difference};

    return returnvec;

}

dmat link_jacobian(double phi, double zeta1, double zeta2, double log_both, double log_scale, double logit_rho,
        double psi) {

    dmat returnmat = arma::zeros<dmat>(4,7);

    // Calculate the first row.
    double beta_logit_rho, beta_log_scale, beta_psi;
    auto result1 = beta_gradient(zeta1, phi, zeta2, logit_rho, log_scale, psi); 
    std::tie(beta_logit_rho, beta_log_scale, beta_psi)  = result1;

    returnmat(0,0) = 1;
    returnmat(0,3) = -1 * beta_log_scale;
    returnmat(0,4) = -1 * beta_logit_rho; 
    returnmat(0,5) = -1 * beta_psi;


    // Calculate the second row. //
    double gamma_log_both, gamma_log_scale, gamma_psi;
    auto result2 = gamma_gradient(zeta1, phi, zeta2, log_both, log_scale, psi); 
    std::tie(gamma_log_both, gamma_log_scale, gamma_psi) = result2;

    returnmat(1,1) = 1; 
    returnmat(1,2) = -1 * gamma_log_both;
    returnmat(1,3) = -1 * gamma_log_scale;
    returnmat(1,5) = -1 * gamma_psi;


    // Calculate the third row.
    double psi_log_scale, psi_logit_rho;
    auto result3 = psi_gradient(phi, log_scale, logit_rho); 
    std::tie(psi_log_scale, psi_logit_rho) =  result3;
    returnmat(2,3) = - psi_log_scale;
    returnmat(2,4) = -1 * psi_logit_rho;
    returnmat(2,5) = 1;
    
    // Calculate the fourth row.
    returnmat(3,6) = -1; 

    return returnmat;
}

dmat covariance_kernel(double phi1, double zeta1_1, double zeta2_1, double phi2,
        double zeta1_2, double zeta2_2,  double log_both, double log_scale, double
        logit_rho, const dmat& omega_cov, double psi) {

    dmat link_grad_left = link_jacobian(phi1,  zeta1_1,  zeta2_1, log_both,
            log_scale, logit_rho,  psi); 

    dmat link_grad_right = link_jacobian(phi2,  zeta1_2,  zeta2_2, log_both,
            log_scale, logit_rho,  psi); 

    return link_grad_left * omega_cov * link_grad_right.t();

}

double qlr_in(const std::vector<double>& prices, const
        std::unordered_map<std::string, double>& omega, const dmat& omega_cov)
{ 

    dvec prices_vec = dvec(prices);
    double returnval = arma::datum::inf;

    if (not prices_vec.is_finite()) {
        return returnval;
    }

    dvec link_in = link_total(prices_vec(0), prices_vec(1), prices_vec(2),
            omega.at("beta"s), omega.at("gamma"s), omega.at("log_both"s),
            omega.at("log_scale"s), omega.at("logit_rho"s), omega.at("psi"s),
            omega.at("xi"s)); 

    dmat cov_prices = covariance_kernel(prices_vec(0), prices_vec(1),
            prices_vec(2), prices_vec(0), prices_vec(1), prices_vec(2),
            omega.at("log_both"s), omega.at("log_scale"s), omega.at("logit_rho"s),
            omega_cov, omega.at("psi"s)); 

    if (
        (cov_prices.is_finite() && link_in.is_finite()) 
        && 
        (cov_prices.max() < 1e10 && link_in.max() < 1e10)
        &&
        (cov_prices.min() > -1e10 && link_in.min() > -1e10)
    ) {
      
        // We do not need to pre-multiply by $T$ because we are using scaled
        // versions of the covariances.

        try {
            dmat right_hand_side = arma::solve(cov_prices, link_in);
            returnval = arma::as_scalar(link_in.t() * right_hand_side);
        }
        catch (std::exception& e)
        {
            returnval = arma::datum::inf;
        }
    }

    return returnval;
}

double qlr_in_star(const std::vector<double>& prices, const dvec& innov, const
        std::unordered_map<std::string, double>& omega, const dmat& omega_cov,
        const std::vector<double>& true_prices) {

    dvec prices_vec = dvec(prices);
    dvec true_prices_vec = dvec(true_prices);
    double returnval = arma::datum::inf;

    if (not prices_vec.is_finite()) {
        return returnval;
    }

    auto link_in = [=] (const dvec& prices) {
        return link_total(prices(0), prices(1), prices(2), omega.at("beta"s),
                omega.at("gamma"s), omega.at("log_both"s),
                omega.at("log_scale"s), omega.at("logit_rho"s),
                omega.at("psi"s), omega.at("xi"s)); 
    };

    dvec link_true = link_in(true_prices_vec); 
    dvec link_test = link_in(prices_vec); 

    auto cov_params = [&] (const dvec& prices1, const dvec& prices2) { 
        return  covariance_kernel(prices1(0), prices1(1), prices1(2),
                prices2(0), prices2(1), prices2(2), omega.at("log_both"s),
                omega.at("log_scale"s), omega.at("logit_rho"s), omega_cov,
                omega.at("psi"s)); 
    }; 

    auto cov_params_true = [&] (const dvec& prices1) { return cov_params(prices1, true_prices_vec);};
    dmat cov_prices = cov_params(prices_vec, prices_vec); 

    if (link_true.is_finite() && link_test.is_finite()) {

        try {

            dmat root_cov_true_true = arma::chol(cov_params_true(true_prices_vec), "lower");
            dvec innov_sim = root_cov_true_true * innov;
            
            // We condition on the residual process.
            dvec residual = (link_test - cov_params_true(prices_vec) * cho_solve(root_cov_true_true, link_true));
            dvec link_sim = (residual + cov_params_true(prices_vec) * cho_solve(root_cov_true_true, innov_sim));

            // We do not need to pre-multiply by $T$ because we are using
            // scaled versions of the covariances.
            returnval = arma::as_scalar(link_sim.t() * arma::solve(cov_prices, link_sim)); 

        } catch (const std::runtime_error& e) {
            returnval = arma::datum::inf;
        }

    }

    if (not std::isfinite(returnval)) {
        returnval = arma::datum::inf;
    }

    return returnval;
}

PYBIND11_MODULE(libvolpriceinference, m) {

    m.def("_simulate_autoregressive_gamma", &simulate_autoregressive_gamma, stream_redirect(), 
          "This function provides draws from the ARG(1) process of Gourieroux & Jaiak",
          "delta"_a=1, "rho"_a=0, "scale"_a=.1, "time_dim"_a=100, "initial_point"_a=.1); 
    
    m.def("_threadsafe_gaussian_rvs", &threadsafe_gaussian_rvs, stream_redirect(), 
          "This function provides a vector of Gaussian random variates that are drawn in a thread safe manner.",
          "time_dim"_a=100); 

    m.def("A_func", &A_func, stream_redirect(), "This function computes function A() in the accompanying paper.",
            "x"_a, "logit_rho"_a, "log_scale"_a);

    m.def("B_func", &B_func, stream_redirect(), "This function computes function B() in the accompanying paper.",
            "x"_a, "log_both"_a, "log_scale"_a);

    m.def("alpha_func", &alpha_func, stream_redirect(), "This function computes function C() in the accompanying paper.",
            "x"_a, "phi"_a, "psi"_a);

    m.def("compute_beta", &compute_beta, stream_redirect(), 
            "This function computes the first link function in the accompanying paper.",
            "zeta1"_a, "phi"_a, "zeta2"_a, "logit_rho"_a, "log_scale"_a, "psi"_a);

    m.def("beta_curvature", &beta_curvature, stream_redirect(), 
            "This function computes curvature of beta_omega in the accompanying paper.",
            "phi"_a, "logit_rho"_a, "log_scale"_a, "psi"_a);

    m.def("compute_gamma", &compute_gamma, stream_redirect(), 
            "This function computes the second link function in the accompanying paper.",
            "zeta1"_a, "phi"_a, "zeta2"_a, "log_both"_a, "log_scale"_a, "psi"_a);

    m.def("gamma_curvature", &gamma_curvature, stream_redirect(), 
            "This function computes curvature of gamma_omega in the accompanying paper.",
            "phi"_a, "log_both"_a, "log_scale"_a, "psi"_a);

    m.def("compute_psi", &compute_psi, stream_redirect(), 
            "This function computes function the second link function in the accompanying paper.",
            "phi"_a, "zeta2"_a, "log_scale"_a, "logit_rho"_a);

    m.def("link_total", &link_total, stream_redirect(),
            "This function computes the link function.",
            "phi"_a, "zeta1"_a, "zeta2"_a, "beta"_a, "gamma"_a, "log_both"_a,  "log_scale"_a,  "logit_rho"_a, 
            "psi"_a, "xi"_a); 

    m.def("beta_gradient", &beta_gradient, stream_redirect(),
            "This function computes the gradient of the beta link function.", 
            "zeta1"_a, "phi"_a, "zeta2"_a, "logit_rho"_a, "log_scale"_a, "psi"_a); 

    m.def("gamma_gradient", &gamma_gradient, stream_redirect(),
            "This function computes the gradient of the gamma link function.", 
            "zeta1"_a, "phi"_a, "zeta2"_a, "log_both"_a, "log_scale"_a, "psi"_a); 

    m.def("link_jacobian", &link_jacobian, stream_redirect(),
            "This function computes the jacobian of the link function.", "phi"_a, "zeta1"_a, "zeta2"_a, "log_both"_a,
            "log_scale"_a,  "logit_rho"_a, "psi"_a); 

    m.def("A_diff2", &A_diff2, stream_redirect(), 
        "This function computes the derivative of A() with respect to logit_rho", "x"_a, "logit_rho"_a, 
        "log_scale"_a);

    m.def("B_diff3", &B_diff3, stream_redirect(), 
        "This function computes the derivative of B() with respect to log_scale", 
        "x"_a, "log_both"_a, "log_scale"_a);

    m.def("gamma_curvature_diff3", &gamma_curvature_diff3, stream_redirect(), 
            " The derivative of gamma curvature with respect to log_scale.", 
            "log_both"_a, "log_scale"_a, "psi"_a);

    m.def("_covariance_kernel", &covariance_kernel, stream_redirect(), 
          "This function computes the covariance kernel defined in the accompanying paper", 
          "phi1"_a, "zeta1_1"_a, "zeta2_1"_a, "phi2"_a, "zeta1_2"_a, "zeta2_2"_a,
          "log_both"_a, "log_scale"_a, "logit_rho"_a, "omega_cov"_a, "psi"_a); 

    m.def("_qlr_in", &qlr_in, stream_redirect(), 
            "This function computes the qlr function. The prices vector and the omega_cov matrix should have the rows/columns in alphabetical order.",
            "prices"_a, "omega"_a, "omega_cov"_a);

    m.def("_qlr_in_star", &qlr_in_star, stream_redirect(), 
            "This function computes the qlr-star function. The prices vector and the omega_cov matrix should have the rows/columns in alphabetical order.",
            "prices"_a, "innov"_a, "omega"_a, "omega_cov"_a, "true_prices"_a);

}
